package leafTaps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hero_Radiobutton {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("Radio").click();
		driver.findElementByXPath("//input[@id='yes']").click();
		List<WebElement> all = driver.findElementsByXPath("//input[@name='news']");
		System.out.println(all.size());
		for(int i=0;i<all.size();i++)
		{
			if(all.get(i).isSelected())
			{
				System.out.println(all.get(i).getAttribute("text"));
				System.out.println("Success");
			}
		}
		
		
		
	}

}
