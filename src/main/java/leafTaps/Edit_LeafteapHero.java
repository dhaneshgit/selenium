package leafTaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

public class Edit_LeafteapHero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//h5[text()='Edit']").click();
		driver.findElementById("email").sendKeys("dhaneshit@gmail.com");
		String text2 = driver.findElementByXPath("(//div[@class='row'])[1]/child::div/label").getText();
		System.out.println(text2);
		driver.findElementByXPath("((//div[@class='row'])[2])/child::div/input").sendKeys("Add",Keys.TAB);
		String text = driver.findElementByXPath("(//input[@name='username'])[1]").getAttribute("value");
		System.out.println(text);
		driver.findElementByXPath("(//input[@name='username'])[2]").clear();
		boolean b = driver.findElementByXPath("((//div[@class='row'])[5])/child::div/input").isEnabled();
		System.out.println(b);
	}

}
