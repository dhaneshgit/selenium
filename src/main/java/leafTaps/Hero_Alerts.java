package leafTaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hero_Alerts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("Aler").click();
		driver.findElementByXPath("//button[text()='Alert Box']").click();
		Alert alt = driver.switchTo().alert();
		alt.accept();
		driver.findElementByXPath("//button[text()='Confirm Box']").click();
		alt.accept();
		String text = driver.findElementById("result").getText();
		System.out.println(text);
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		alt.sendKeys("Yes, testleaf"); alt.accept();
		String text1 = driver.findElementById("result1").getText();
		System.out.println(text);
		driver.findElementByXPath("//button[text()='Line Breaks?']").click();
		String text2 = alt.getText(); System.out.println(text2);
		alt.accept();
		
		
		
		

	}

}
