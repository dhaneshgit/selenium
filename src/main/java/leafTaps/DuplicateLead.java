package leafTaps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		//Have doubt on below X-Path
		driver.findElementByXPath("//div[@id='label']/a").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//span[@class='x-tab-strip-inner'])[3]").click();
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("kumar@gmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		String Name = driver.findElementByXPath("(//a[text()='Pooja'])[1]").getText();
		
		System.out.println(Name);
		driver.findElementByXPath("(//a[text()='Pooja'])[1]").click();
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		String title = driver.getTitle();
		if(title.contains("Duplicate Lead"))
		{
			System.out.println("Correct title " + title);
		}
		else
		{
			System.out.println("Update the title properly");
		}
//		driver.findElementByXPath("//a[text()='Create Lead']").click();
		
		WebElement Leadname = driver.findElementByXPath("//input[@id='createLeadForm_firstName']");
		String text2 = Leadname.getText();
		System.out.println(text2);
		if(text2.equals(Name))
		{
			System.out.println("Same name");
		}
		else
		{
			System.out.println("Not same name");
		}
		
	}

}
