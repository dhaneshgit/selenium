package leafTaps;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class HeroImage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("Image").click();
//		driver.findElementByXPath("//div[@class='row']/child::div/img").click();
//		List<WebElement> img = driver.findElementsByTagName("img");
//		System.out.println(img.size());
//		for (WebElement a : img) {
//		String imgg = a.getAttribute("src");
//		Verifylink(imgg);
//		System.out.println(imgg);
		WebElement ele = driver.findElementByXPath("(//div[@class='row'])[3]/child::div/img");
		Actions act=new Actions(driver);
		act.moveToElement(ele).perform();
		act.click();
		
		
		
	}
	
		public static void Verifylink(String imgg)
		{
			
				try {
					URL url = new URL(imgg);
					HttpURLConnection connect = (HttpURLConnection) url.openConnection();
					connect.setConnectTimeout(3000);
					connect.connect();
					if(connect.getResponseCode()<400)
					{
						System.out.println(imgg+" - "+connect.getResponseMessage());
					}
					if(connect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)
					{
						System.out.println(imgg+" - "+connect.getResponseMessage()+ " - "+HttpURLConnection.HTTP_NOT_FOUND);
					}
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
	
}
