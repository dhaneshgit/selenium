package leafTaps;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HeroLinks {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("Link").click();
//		driver.findElementByXPath("(//a[contains(text(),'Go to Home Page')])[1]").click();
		String string = driver.findElementByLinkText("Find where am supposed to go without clicking me?").getAttribute("href");
		System.out.println(string);
		List<WebElement> all = driver.findElementsByTagName("a");
		System.out.println(all.size());
		for (WebElement a : all) {
			String url = a.getAttribute("href");
			Verifylink(url);
			System.out.println(url);
			
		}}
			public static void Verifylink(String Lurl)
			{
				
					try {
						URL url = new URL(Lurl);
						HttpURLConnection connect = (HttpURLConnection) url.openConnection();
						connect.setConnectTimeout(3000);
						connect.connect();
						if(connect.getResponseCode()==200)
						{
							System.out.println(Lurl+" - "+connect.getResponseMessage());
						}
						if(connect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)
						{
							System.out.println(Lurl+" - "+connect.getResponseMessage()+ " - "+HttpURLConnection.HTTP_NOT_FOUND);
						}
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				}
			
	}


