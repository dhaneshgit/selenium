package leafTaps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Hero_CheckBox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("Check").click();
		driver.findElementByXPath("(//div[@class='example'])[1]/input[1]").click();
		WebElement button = driver.findElementByXPath("(//div[@class='example'])[2]/input[1]");
		if(button.isSelected())
		{
			System.out.println("Selected");
		}
		List<WebElement> deselect = driver.findElementsByXPath("(//div[@class='example'])[3]/input");
		
		for (int i = 0; i < deselect.size(); i++) {
			if(deselect.get(i).isSelected())
			{
				deselect.get(i).click();
				System.out.println("cleared");
			}
		}
		List<WebElement> all = driver.findElementsByXPath("(//div[@class='example'])[4]/input");
			for (int j = 0; j < all.size(); j++) {
				all.get(j).click();		
			}
		}
	}


