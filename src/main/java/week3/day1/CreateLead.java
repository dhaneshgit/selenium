package week3.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
		driver.findElementByXPath("(//input[@name='parentPartyId'])//parent::*/a/img").click();
		Set<String> Account = driver.getWindowHandles();
		List<String> Acc = new ArrayList<String>();
		Acc.addAll(Account);
		driver.switchTo().window(Acc.get(1));
		driver.findElementByXPath("//input[@name='accountName']").sendKeys("d");
		driver.findElementByXPath("//button[text()='Find Accounts']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//table[@class='x-grid3-row-table']//a").click();
		driver.switchTo().window(Acc.get(0));
		
		
//		driver.findElementById("ext-gen590").click();
//		Thread.sleep(3000);
//		driver.findElementByLinkText("Company").click();
		driver.findElementById("createLeadForm_firstName").sendKeys("Dhaneshkumr");
		driver.findElementById("createLeadForm_lastName").sendKeys("NS");
			
		WebElement Source = driver.findElementByName("dataSourceId");
		Select Sou = new Select(Source);
		Sou.selectByValue("LEAD_OTHER");
		
		
		
////		driver.findElementByClassName("lastName").sendKeys("kumar");
//		driver.findElementByClassName("smallSubmit")
//		driver.findElementByName("submitButton").click();
//		System.out.println(driver.getTitle());
//		WebElement elesource = driver.findElementByName("dataSourceId");
//		Select sel = new Select(elesource);
//		sel.selectByVisibleText("Employee");
		
		WebElement Market = driver.findElementByName("marketingCampaignId");
		Select mark = new Select(Market);
		mark.selectByValue("CATRQ_CARNDRIVER");
		
		driver.findElementByName("firstNameLocal").sendKeys("DK");
		driver.findElementByName("lastNameLocal").sendKeys("NS");
		driver.findElementByName("personalTitle").sendKeys("XXXX");
		
		driver.findElementById("createLeadForm_birthDate-button").click();
		WebElement dob = driver.findElementByXPath("(//div[@class='calendar'])/table");
		List<WebElement> rows = dob.findElements(By.tagName("tr"));
		List<WebElement> columns = rows.get(2).findElements(By.tagName("td"));
		columns.get(4).click();
		
//		driver.findElementByLinkText("26").click();
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("XXX");
		driver.findElementById("createLeadForm_departmentName").sendKeys("X");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000");
		WebElement Currency = driver.findElementById("createLeadForm_currencyUomId");
		Select Curr = new Select(Currency);
		Curr.selectByValue("INR");
		
		WebElement soc = driver.findElementByName("industryEnumId");
		Select soc1 = new Select(soc);
		List<WebElement> All = soc1.getOptions();
		soc1.selectByIndex(All.size()-3);
		for (WebElement M : All) {
			String txt = M.getText();
			if(txt.startsWith("M"))
					{
				System.out.println(txt);
					}
		}
			driver.findElementById("createLeadForm_numberEmployees").sendKeys("40");
			WebElement Ownership = driver.findElementById("createLeadForm_ownershipEnumId");
			Select Owner = new Select(Ownership);
			Owner.selectByVisibleText("Partnership");
			
			driver.findElementById("createLeadForm_sicCode").sendKeys("98769");
			driver.findElementById("createLeadForm_tickerSymbol").sendKeys("@@@@@");
			driver.findElementById("createLeadForm_description").sendKeys("Cool");
			driver.findElementById("createLeadForm_importantNote").sendKeys("Will let you know");
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").clear();
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("3");
			driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("101");
			driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys(" 848591");
			driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("0452");
			driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Lead");
			driver.findElementById("createLeadForm_primaryEmail").sendKeys("test@gmail.com");
			driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaftaps.com/opentaps/control/main");
			driver.findElementById("createLeadForm_generalToName").sendKeys("XXXX");
			driver.findElementById("createLeadForm_generalAttnName").sendKeys("XX");
			driver.findElementById("createLeadForm_generalAddress1").sendKeys("XXXXXXXX");
			driver.findElementById("createLeadForm_generalAddress2").sendKeys("XXXX");
			driver.findElementById("createLeadForm_generalCity").sendKeys("Madurai");
			driver.findElementById("createLeadForm_generalPostalCode").sendKeys("Mdu");
			driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("60032");
			WebElement properties = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
			Select ppts = new Select(properties);
			ppts.selectByValue("DE");
			WebElement Country = driver.findElementById("createLeadForm_generalCountryGeoId");
			Select Cnty = new Select(Country);
			List<WebElement> cty = Cnty.getOptions();
			Cnty.selectByIndex(cty.size()-4);
//			driver.findElementByXPath("(//input[@class='smallSubmit'])").click();
			
				
			}
			
		}
			
		
			
	
			
		
		
		

	


