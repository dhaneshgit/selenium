package week3.day2;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		//Have doubt on below X-Path
		driver.findElementByXPath("//div[@id='label']/a").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("karthi");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		String title = driver.getTitle();
		if(title.contains("View Lead"))
		{
			System.out.println("Correct Title and the title is: "+title);
		}
		driver.findElementByXPath("//a[text()='Edit']").click();
		driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
		driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("Cognizant");
		driver.findElementByXPath("//input[@name='submitButton']").click();
		String text = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText();
		if(text.contains("Cognizant"))
		{
			System.out.println("Company Name Updated Correctly and the name is " + text);
		}
		else
		{
			System.out.println("Update Properly");
		}
	}

}




//public void logintestleaf(String user, String pass)
//{
//	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
//	ChromeDriver driver = new ChromeDriver(); 
//	driver.manage().window().maximize();
//	driver.get("http://leaftaps.com/opentaps/control/main");
//	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//	driver.findElementByXPath("//input[@id='username']").sendKeys(user);
//	driver.findElementByXPath("//input[@id='password']").sendKeys(pass);
//	driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
//	
//}
//
//public void findLeadname(String name) throws Exception
//{
//	driver.findElementByXPath("//div[@id='label']/a").click();
//	driver.findElementByXPath("//a[text()='Leads']").click();
//	driver.findElementByXPath("//a[text()='Find Leads']").click();
//	driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(name);
//	driver.findElementByXPath("//button[text()='Find Leads']").click();
//	Thread.sleep(3000);
//	String title = driver.getTitle();
//	if(title.contains("View Lead"))
//	{
//		System.out.println("Correct Title and the title is: "+title);
//	}
//}
//
//public void Edit()
//{
//	driver.findElementByXPath("//a[text()='Edit']").click();
//	driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
//	driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("Cognizant");
//	driver.findElementByXPath("//input[@name='submitButton']").click();
//	String text = driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText();
//	if(text.contains("Cognizant"))
//	{
//		System.out.println("Company Name Updated Correctly and the name is " + text);
//	}
//	else
//	{
//		System.out.println("Update Properly");
//	}
//		
//}