package week3.day2;

import java.awt.RenderingHints.Key;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU", Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		
		
		List<String> Store = new LinkedList<>();
		WebElement Table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> Rownum = Table.findElements(By.tagName("tr"));
		for (int i = Rownum.size()-1; i >= 0 ; i--) {
			WebElement Rows = Rownum.get(i);
			List<WebElement> Columnno = Rows.findElements(By.tagName("td"));
			String trains=Columnno.get(5).getText();
			System.out.println(trains);
			Store.add(trains);
			}
		driver.findElementByXPath("(//a[text()='MADURAI EXPRESS'])[1]").click();
			WebElement Madurairoute = driver.findElementByXPath("//table[@class='DataTable RouteList']");
			List<WebElement> mdurow = Madurairoute.findElements(By.tagName("tr"));
			for (int i = 22; i < mdurow.size(); i++) {
				WebElement Row = mdurow.get(i);
				List<WebElement> col = Row.findElements(By.tagName("td"));
				String route = col.get(2).getText();
				System.out.println(route);
				Store.add(route);
			}
			
			
		Collections.sort(Store);			
		System.out.println(Store);
//		System.out.println(Store);
		
		
		
	}
}