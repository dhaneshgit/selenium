package codeQuestion;

import java.util.Scanner;

public class CC20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the no");
		int n = sc.nextInt();
		int j = 0;
		for (int i = 1; i < n; i++) {
			if (i % 3 == 0 || i % 5 == 0) {
				System.out.print(i + ",");
				j = j + i;
		}
		}
		System.out.println("");
		System.out.println("Sum is " + j);
	}

}
