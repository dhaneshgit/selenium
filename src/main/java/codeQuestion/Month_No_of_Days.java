package codeQuestion;

import java.util.Scanner;

public class Month_No_of_Days {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			Scanner sc =new Scanner(System.in);
			System.out.println("Enter the month");
			int Months = sc.nextInt();
			System.out.println("Enter the year");
			int year =sc.nextInt();
			switch (Months) {
			case 1:
				System.out.println("Jan");
				System.out.println("No.of.days:31");
				break;
			case 2:
				System.out.println("feb");
				if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
				System.out.println("No.of.days:29");
				else
				System.out.println("no.of.days:28");
				break;
			case 3:
				System.out.println("Mar");
				System.out.println("No.of.days:31");
				break;
			case 4:
				System.out.println("Apr");
				System.out.println("No.of.days:30");
				break;
			case 5:
				System.out.println("May");
				System.out.println("No.of.days:31");
				break;
			case 6:
				System.out.println("Jun");
				System.out.println("No.of.days:30");
				break;
			case 7:
				System.out.println("Jul");
				System.out.println("No.of.days:31");
				break;
			case 8:
				System.out.println("Aug");
				System.out.println("No.of.days:30");
				break;
			case 9:
				System.out.println("Sep");
				System.out.println("No.of.days:31");
				break;
			case 10:
				System.out.println("Oct");
				System.out.println("No.of.days:30");
				break;
			case 11:
				System.out.println("Nov");
				System.out.println("No.of.days:31");
				break;
			case 12:
				System.out.println("Dec");
				System.out.println("No.of.days:30");
				break;

			default:
				System.out.println("Enter correct month");
				break;
			}
	}

}
