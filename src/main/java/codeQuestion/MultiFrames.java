package codeQuestion;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultiFrames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com/pages/frame.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement Pframes = driver.findElementByXPath("//section[@class='innerblock'][1]");
		driver.switchTo().frame(Pframes);
		WebElement childf = driver.findElementByXPath("//div[@id='wrapframe']");.
		driver.switchTo().frame(childf);
		WebElement CFrame1 = driver.findElementByXPath("//div[@id='wrapframe']/iframe");
		driver.switchTo().frame(CFrame1);
		driver.findElementById("Click").click();
		
		
	}

}
