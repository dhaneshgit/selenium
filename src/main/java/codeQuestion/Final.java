package codeQuestion;

public class Final {

//	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 
			 static int speedlimit=90; //final variable  
			 static void run(){  
			  speedlimit = 400; 
			  System.out.println();
			 }  
			 public static void main(String args[]){  
			 Final obj=new Final();  
			 obj.run();  
			 }  
			

	class Bike{  
		  static void run(){System.out.println("running");}  
		}  
		     
		class Honda extends Bike{  
		   void run(){System.out.println("running safely with 100kmph");}  
		     
		   public static void main(String args[]){  
		   Honda honda= new Honda();  
		   honda.run();  
		   }  
		}  
	class Student{  
	     int rollno;  
	     String name;
	     int a;
	     static String college = "ITS";  
	     //static method to change the value of static variable  
	     static void change(){  
	     college = "BBDIT";
	     a=10;
	     }  
	     //constructor to initialize the variable  
	     Student(int r, String n){  
	     rollno = r;  
	     name = n;  
	     }  
	     //method to display values  
	     void display(){System.out.println(rollno+" "+name+" "+college);}  
	}  
	//Test class to create and display the values of object  
	public class TestStaticMethod{  
	    public static void main(String args[]){  
	    Student.change();//calling change method  
	    //creating objects  
	    Final s1 = new Student(111,"Karan");  
	    Student s2 = new Student(222,"Aryan");  
	    Student s3 = new Student(333,"Sonoo");  
	    //calling display method  
	    s1.display();  
	    s2.display();  
	    s3.display();  
	    }  
	}  
	
	
	


