package codeQuestion;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Uncheckthebutton {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com/pages/checkbox.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//div[@class='example'][2]/child::input").click();
		boolean check = driver.findElementByXPath("//div[@class='example'][2]/child::input").isSelected();
		if(check)
		{
			System.out.println("selected and incorrect");
		}
		else
		{
			System.out.println("unselected and correct");
		}
		
		
	}

}
