package codeQuestion;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Table {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://testleaf.herokuapp.com/pages/table.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement Table = driver.findElementByXPath("((//section[@class='innerblock'])/div)/table");
		List<WebElement> allrow = Table.findElements(By.tagName("tr"));
		for (int i = 0; i < allrow.size(); i++) {
			WebElement row = allrow.get(i);
			List<WebElement> data = row.findElements(By.tagName("td"));
			String percentage = data.get(1).getText();
			System.out.println(percentage);
			
			
		}
		
		
	}

}
