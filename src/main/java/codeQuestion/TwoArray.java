package codeQuestion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class TwoArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a= {1,2,4,5,8};
		int b[] = {1,2,13};
		Set<Integer> ADD = new LinkedHashSet<>();
		for (int i = 0; i < b.length; i++) {
			ADD.add(b[i]);
		}
		for (int i = 0; i < a.length; i++) {
			ADD.add(a[i]);
		}
		List<Integer> add = new ArrayList<>();
		add.addAll(ADD);
		Collections.sort(add);
		System.out.println(add);
		
	}

}
