package codeQuestion;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Multiwindow {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.quora.com/Why-does-Java-not-support-destructors");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElementByXPath("(//a[@class='question_link'])[1]").click();	
		Set<String> windowHandles = driver.getWindowHandles();s
		List<String> all = new ArrayList<String>();
		all.addAll(windowHandles);
		driver.findElementByXPath("//span[@class='smaller_button']/span/a").click();
		windowHandles = driver.getWindowHandles();
		all.addAll(windowHandles);
		driver.findElementByXPath("//input[@id='identifierId']").sendKeys("bye");
		driver.switchTo().window(all.get(1));
		driver.findElementByXPath("//a[@class='facebook_button submit_button']").click();
		windowHandles = driver.getWindowHandles();
		all.addAll(windowHandles);
		driver.switchTo().window(all.get(0)).close();
		driver.switchTo().window(all.get(1)).close();
		
		
		
		
		
	}

}
