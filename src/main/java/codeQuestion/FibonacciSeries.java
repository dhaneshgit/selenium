package codeQuestion;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		int a = 0, b = 1, temp = 0;

		Scanner s = new Scanner(System.in);
		System.out.println("Enter the elements");
		int n = s.nextInt();
		// System.out.println("Enter the first two elements");
		for (int i = 1; i < n; i++) {
			{
				System.out.println(a);
				temp = a + b;
				a = b;
				b = temp;
			}
		}
	}
}

// using array --
//			int n = 0;
//			int p[]=new int[k];
//			p[0]=0; p[1]=1;
//			for (int i = 2; i < k; i++) {
//				p[i]=p[i-1]+p[i-2];
//			}
//			for (int i = 0; i < k; i++) {
//				System.out.println(p[i]);
//			}
//			
//		}
//		//System.out.println(temp);
//	}
