package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Contact Us").click();
		System.out.println(driver.getTitle());
		
		Set<String> allwindow = driver.getWindowHandles();
		List<String> ls = new ArrayList<String>();
		ls.addAll(allwindow);
		driver.switchTo().window(ls.get(1));
		System.out.println(driver.getTitle());
		
		driver.switchTo().window(ls.get(0)).close();
		
		
	}

}
