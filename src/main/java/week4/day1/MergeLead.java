package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		driver.findElementByXPath("//div[@id='label']/a").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//a[text()='Merge Leads']").click();
		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdFrom'])//parent::*/following-sibling::a/img")
				.click();
		Set<String> Win2 = driver.getWindowHandles();
		List<String> Win2list = new ArrayList<String>();
		Win2list.addAll(Win2);
		driver.switchTo().window(Win2list.get(1));
		driver.findElementByXPath("(//div[@class='x-form-item x-tab-item'])//input").sendKeys("10089");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//a[text()='10089']").click();
		driver.switchTo().window(Win2list.get(0));
		driver.findElementByXPath("(//table[@id='widget_ComboBox_partyIdTo'])//parent::*/following-sibling::a/img")
				.click();
		Set<String> Wino = driver.getWindowHandles();
		List<String> allwin = new ArrayList<>();
		allwin.addAll(Wino);
		driver.switchTo().window(allwin.get(1));
		driver.findElementByXPath("(//div[@class='x-form-item x-tab-item'])//input").sendKeys("10089");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		driver.findElementByXPath("//a[text()='10089']").click();
		driver.switchTo().window(allwin.get(0));
		driver.findElementByXPath("//a[text()='Merge']").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		String Errormessage = driver.findElementByXPath("//li[@class='errorMessage']").getText();
		System.out.println(Errormessage);
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementByXPath("(//div[@class='x-form-item x-tab-item'])//input").sendKeys("10089");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
//		WebElement Message = driver.findElementByXPath("//div[@class='x-paging-info']");
//		String text = Message.getText();
//		if (text.contains("No records to display")) {
//			System.out.println("Merged");
//		} else {
//			System.out.println("Not Merged");
//		}
		System.out.println("Merged cannot be possible for same ID");
	}

}
