package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alerts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement frame = driver.findElementById("iframeResult");
//		driver.find
		driver.switchTo().frame(frame);
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert1 = driver.switchTo().alert();
		alert1.sendKeys("Dhanesh");
		alert1.accept();
		String message = driver.findElementById("demo"
				+ "").getText();
		if(message.contains("Dhanesh"))
		{
			System.out.println("Verified");
		}
		else
		{
			System.out.println("Not verified");
		}
		
	}

}
