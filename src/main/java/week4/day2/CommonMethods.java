package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xddf.usermodel.text.UnderlineType;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.bs.A;

public class CommonMethods {

	public void allMethods() throws IOException {
		
		// browser invoke
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("");

		// dropdown by visibletext/Value/Index

		WebElement Dropdown = driver.findElementByXPath("");
		Select allDropdown = new Select(Dropdown);
		allDropdown.selectByVisibleText("Text");
		allDropdown.selectByValue("");
		allDropdown.selectByIndex(0);
		
		

		// Count of the dropdown

		List<WebElement> all = allDropdown.getOptions();
		all.size();

		// read the text

		String text = driver.findElementById("").getText();

		// get URL

		driver.getCurrentUrl();

		// Text is enabled ?

		driver.findElementByXPath("").isEnabled();

		driver.findElementByXPath("").isSelected();

		// window switch

		Set<String> window = driver.getWindowHandles();
		List<String> allwindow = new ArrayList<>();
		allwindow.addAll(window);
		driver.switchTo().window(allwindow.get(1));

		File Snapshot = driver.getScreenshotAs(OutputType.FILE);
		File f1 = new File("Path of the file which should be save");
		FileUtils.copyFile(f1, Snapshot);

		// Alert
		Alert alert = driver.switchTo().alert();
		alert.accept();
		alert.dismiss();
		alert.sendKeys("");
		alert.getText();

		// Frames
		WebElement frames = driver.findElementByXPath("");
		driver.switchTo().frame(frames);
		driver.findElementByXPath("").click();

		// parent frame (Page contains one frame

		driver.switchTo().parentFrame();

		driver.switchTo().defaultContent();

		// webtable
		// reading the 4th row and 3rd column

		WebElement table = driver.findElementByXPath("");
		List<WebElement> allrows = table.findElements(By.tagName("tr"));
		List<WebElement> allcolumns = allrows.get(3).findElements(By.tagName("td"));
		allcolumns.get(2).getText();

		// waits

		// -- Implicityly wait

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement wait1 = driver.findElementByXPath("");
		driver.findElementsByXPath("");

		// Explicitly wait

		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Webdriver wait

		WebDriverWait wait = new WebDriverWait(driver, 230);
		wait.until(ExpectedConditions.elementToBeClickable(wait1));

	}

}
