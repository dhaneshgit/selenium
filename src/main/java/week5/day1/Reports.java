package week5.day1;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	@Test
	public void runreport()
	{	
		//create our html report
		html = new ExtentHtmlReporter("./report/extentReport.html");
		
		//Editing our html reports
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test = extent.createTest("TC001_login", "Login into leaftaps");
		test.assignAuthor("Dhanesh");
		test.assignCategory("Smoke");
		
		try {
			test.fail("Enter", MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		extent.flush();
		
		
	}
}
